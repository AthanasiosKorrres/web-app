package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.enums.UserRole;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class EditOwnerController {
    private static final String OWNER_ATTR = "owner";
    private static final String OWNERS_PROPERTY_TYPE = "propertyType";
    private static final String OWNERS_ROLE ="ownersRole";

    @Autowired
    private OwnerService ownerService;

    @PostMapping(value = "/owners/{taxRegistryNumber}/delete")
    public String deleteOwner(@PathVariable Long taxRegistryNumber) {
        ownerService.deleteByTaxRegistryNumber(taxRegistryNumber);
        return "redirect:/admin/owners";
    }

    @GetMapping(value = "/owners/{taxRegistryNumber}/edit")
    public String editOwner(@PathVariable Long taxRegistryNumber, Model model) {
        OwnerModel ownerModel = ownerService.findOwnerByTaxRegistryNumber(taxRegistryNumber).get(0);
        model.addAttribute(OWNER_ATTR, ownerModel);
        model.addAttribute(OWNERS_PROPERTY_TYPE, PropertyType.values());
        model.addAttribute(OWNERS_ROLE, UserRole.values());
        return "pages/owners_edit";
    }

    @PostMapping(value = "/owners/edit")
    public String editOwner(OwnerModel ownerModel) {
        ownerService.updateOwner(ownerModel);
        return "redirect:/admin/owners";
    }
}
