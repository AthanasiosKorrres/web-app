package com.codehub.projectfuture.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping(value = "/error/generic")
    public String errorPage(Model model) {
        return "error";
    }

    @GetMapping(value = "/access-denied")
    public String accessDeniedPage(Model model) {
        return "/access_denied";
    }

}
