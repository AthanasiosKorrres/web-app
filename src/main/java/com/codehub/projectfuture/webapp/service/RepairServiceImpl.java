package com.codehub.projectfuture.webapp.service;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import com.codehub.projectfuture.webapp.mapper.RepairToRepairModelMapper;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private RepairToRepairModelMapper mapper;

    @Autowired
    private OwnerService ownerService;

    @Override
    public Repair createRepair(Repair repair) {
        return repairRepository.save(repair);
    }

    @Override
    public void deleteById(Long id) {
        repairRepository.deleteById(id);
    }

    @Override
    public Repair updateRepair(RepairModel repairModel) {
        Repair originalRepair = repairRepository.findById(repairModel.getId()).get();
        originalRepair.setDate(repairModel.getDate());
        originalRepair.setStatus(Status.valueOf(repairModel.getStatus()));
        originalRepair.setFixType(FixType.valueOf(repairModel.getFixType()));
        originalRepair.setCost(repairModel.getCost());
        originalRepair.setAddress(repairModel.getAddress());
        originalRepair.setDescription(repairModel.getDescription());
        return repairRepository.save(originalRepair);
    }

    @Override
    public List<RepairModel> findAll() {
        return repairRepository
                .findAll()
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<RepairModel> findRepair(Long id) {
        return repairRepository
                .findById(id)
                .map(repair -> mapper.mapToRepairModel(repair));
    }

    @Override
    public List<RepairModel> findByDate(LocalDate date) {
        return repairRepository
                .findByDate(date)
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<RepairModel> findByOwnerTaxRegistryNumber(Long taxRegistryNumber) {
        return repairRepository.findByOwnerTaxRegistryNumber(taxRegistryNumber).stream().map(repair -> mapper.mapToRepairModel(repair)).collect(Collectors.toList());
    }
    public List<RepairModel> findByDateBetween(LocalDate date,LocalDate date1)
    {
        return repairRepository
                .findByDateBetween(date,date1)
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    @Override
    public List<RepairModel> findByOwner(String name) {
        Owner owner = ownerService.findByEmail(name);
        return repairRepository.findByOwnerTaxRegistryNumber(owner.getTaxRegistryNumber())
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }
}
