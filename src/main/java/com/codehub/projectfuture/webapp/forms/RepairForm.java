package com.codehub.projectfuture.webapp.forms;

import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import net.bytebuddy.implementation.bytecode.assign.TypeCasting;
import org.apache.tomcat.jni.Local;

import java.awt.*;
import java.time.LocalDate;

public class RepairForm {
    private String id;
    private String date;
    private String status;
    private String address;
    private String ownerTaxRegistryNumber;
    private String fixType;
    private String cost;
    private String description;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {return address; }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwnerTaxRegistryNumber() {
        return ownerTaxRegistryNumber;
    }

    public void setOwnerTaxRegistryNumber(String ownerTaxRegistryNumber) {
        this.ownerTaxRegistryNumber = ownerTaxRegistryNumber;
    }

    public String getFixType() {
        return fixType;
    }

    public void setFixType(String fixType) {
        this.fixType = fixType;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RepairForm(String id, String date, String status,String address, String ownerTaxRegistryNumber, String fixType, String cost, String description) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.address = address;
        this.ownerTaxRegistryNumber = ownerTaxRegistryNumber;
        this.fixType = fixType;
        this.cost = cost;
        this.description = description;
    }
    public RepairForm() {

    }
}

