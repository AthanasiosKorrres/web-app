package com.codehub.projectfuture.webapp.mapper;

import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import com.codehub.projectfuture.webapp.model.RepairModel;
import org.springframework.stereotype.Component;

@Component
public class RepairToRepairModelMapper {

    public RepairModel mapToRepairModel(Repair repair) {
        RepairModel repairModel = new RepairModel();
        repairModel.setId(repair.getId());
        repairModel.setDate(repair.getDate());
        repairModel.setStatus(repair.getStatus().getFullName());
        repairModel.setFixType(repair.getFixType().getFullName());
        repairModel.setCost(repair.getCost());
        repairModel.setAddress(repair.getAddress());
        repairModel.setOwnerTaxRegistryNumber(repair.getOwner().getTaxRegistryNumber().toString());
        repairModel.setDescription(repair.getDescription());
        return repairModel;
    }
}

