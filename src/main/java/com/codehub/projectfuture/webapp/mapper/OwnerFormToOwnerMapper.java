package com.codehub.projectfuture.webapp.mapper;

import com.codehub.projectfuture.webapp.SecurityConfig;
import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.enums.UserRole;
import com.codehub.projectfuture.webapp.forms.OwnerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OwnerFormToOwnerMapper {

    @Autowired
    private SecurityConfig securityConfig;

    public Owner toOwner(OwnerForm ownerForm) {
        Owner owner = new Owner();
        owner.setTaxRegistryNumber(Long.parseLong(ownerForm.getTaxRegistryNumber()));
        owner.setAddress(ownerForm.getAddress());
        owner.setEmail(ownerForm.getEmail());
        owner.setFirstName(ownerForm.getFirstName());
        owner.setLastName(ownerForm.getLastName());
        owner.setPassword(securityConfig.passwordEncoder().encode(ownerForm.getPassword()));
        owner.setPhoneNumber(ownerForm.getPhoneNumber());
        owner.setType(PropertyType.valueOf(ownerForm.getType()));
        owner.setRole(UserRole.valueOf(ownerForm.getRole()));


        return owner;
    }
}
